
[install go]: http://golang.org/install.html "Install Go"
[the godoc url]: http://localhost:6060/pkg/github.com/vendion@gamil.com/gsnappy/ "the Godoc URL"
[snappy]: http://code.google.com/p/snappy/ "Snappy project page"
[snappy-go]: http://code.google.com/p/snappy-go/ "snappy-go"

About gsnappy
=============

gsnappy is an archive tool using the Snappy compression library developed by
Google.  Snappy focuses on speed and reasonable compression over anything else,
for more information on Snappy see the [snappy][].

Documentation
=============

Usage
-----

Run gsnappy with the command

    gsnappy [options] file

Prerequisites
-------------

1. [Install Go][].
2. [snappy-go][].

Installation
-------------

    go get github.com/vendion/gsnappy

General Documentation
---------------------

Use `go doc` to vew the documentation for gsnappy

    go doc github.com/vendion/gsnappy

Or alternatively, use a godoc http server

    godoc -http=:6060

and visit [the Godoc URL][]


Author
======

vendion &lt;vendion@gmail.com&gt;

Copyright & License
===================

Copyright (c) 2012, vendion.
All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.
