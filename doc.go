// Copyright 2012, vendion. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

/*  Filename:    doc.go
 *  Author:      vendion <vendion@vendion.net>
 *  Created:     2012-05-23 21:52:33.798219 -0400 EDT
 *  Description: Godoc documentation for gsnappy
 */


/*
gsnappy does...

Usage:

    gsnappy [options] ARGUMENT ...

Arguments:

Options:

*/
package documentation
