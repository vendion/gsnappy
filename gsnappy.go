// Copyright 2012, vendion. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

/*  Filename:    gsnappy.go
 *  Author:      vendion <vendion@gmail.com>
 *  Created:     2012-05-23 21:52:33.797385 -0400 EDT
 *  Description: Main source file in gsnappy
 */

import (
 	"code.google.com/p/snappy-go/snappy"
	"os"
	"fmt"
)

type file struct {
	Name string
	Dir  bool
	EncName string
}

var opt Options

func init() {
	opt = parseFlags()
}

func main() {
	f := file{Name: "", Dir: false}
	f.Name = opt.Args[0]
	if f.Name != "" {
		//Determine if fname is a file or directory
		fi, err := os.Stat(f.Name)
		if err != nil {
			fmt.Printf("%s does not exit!\n", f.Name)
			os.Exit(2)
		}

		mode := fi.Mode()
		if mode & os.ModeSymlink == os.ModeSymlink {
			fmt.Printf("%s is a symbolic link\n", f.Name)
			os.Exit(2)
		}

		if fi.IsDir() {
			f.Dir = true
		}

		oldfile, err := os.Open(f.Name)
		if err != nil {
			fmt.Printf("Error: %s\n", err.Error())
			os.Exit(2)
		}

		buffer := make([]byte, 100)
		//for n, e := oldfile.Read(buffer); e == nil; n, e = oldfile.Read(buffer) {
		//	if n > 0 {
		//		os.Stdout.Write(buffer[0:n])
		//	}
		//}
		_, err = oldfile.Read(buffer)
		if err != nil {
			fmt.Printf("Error reading file %s: %s", f.Name, err)
			os.Exit(2)
		}
		newfile := make([]byte, 100)
		_, err = snappy.Encode(buffer, newfile)
		if err != nil {
			fmt.Printf("Errer compressing file: %s", f.Name)
			os.Exit(2)
		}

		f.EncName = f.Name + ".sz"
		export, err := os.OpenFile(f.EncName, os.O_WRONLY|os.O_CREATE, 0644) //Should keep 
		//original perms
		export.Write(newfile)
		export.Close()
 
		os.Exit(0)
	} else {
		//Should never get here but just in-case
		fmt.Println("No arguments given!")
		os.Exit(2)
	}
}
